import java.util.Date;
import java.util.Map;

import utils.Domains;
import exceptions.NoPermissionsException;
import exceptions.NoSuchFileException;
import exceptions.NoSuchPathException;
import exceptions.ServerOfflineException;
import fileUtils.FileInfo;

public class TesterSync {

	public static void main(String[] args) {
		RemoteFileSystemClient fsc = new RemoteFileSystemClient("guimtr", null);

		try {
			FileInfo f = fsc.getAttr("c", "guimtr", "teste/def");
			System.out.println(f);
		} catch (ServerOfflineException | NoPermissionsException
				| NoSuchFileException | NoSuchPathException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// testLastModified(fsc);
		// testGetFiles(fsc);
		// testIsLocal();

	}

	public static void testIsLocal() {
		System.out.println(Domains.isLocal("a.local"));
		System.out.println(Domains.isLocal(".sync"));
		System.out.println(Domains.isLocal("a.local.txt"));
		System.out.println(Domains.isLocal("abc"));
	}

	public static void testLastModified(RemoteFileSystemClient fsc) {
		Date d = fsc.lastModified(null, null, "./client");
		System.out.println(d.toString());
	}

	public static void testGetFiles(RemoteFileSystemClient fsc) {
		try {
			Map<String, Date> files = fsc.getFiles(null, null, "./client");
			for (String file : files.keySet()) {
				System.out.println(file + " last modified in "
						+ files.get(file));
			}
		} catch (ServerOfflineException | NoPermissionsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
